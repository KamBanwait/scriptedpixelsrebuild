import tailwindcss from "@tailwindcss/vite";

export default defineNuxtConfig({
  ssr: false,

  runtimeConfig: {
    public: {
      sentry: {
        dsn: process.env.SENTRY_DSN,
        environment: process.env.SENTRY_ENVIRONMENT
          ? process.env.SENTRY_ENVIRONMENT
          : "development",
      },
      // all options can be found here: https://www.npmjs.com/package/logrocket?activeTab=code
      // dist/types.d.ts --> interface IOptions
      logRocket: {
        id: 'scriptedpixelsltd/main-website',
        dev: false,
        enablePinia: true,
        config: {},
      },
    },
  },

  app: {
    pageTransition: { name: "page", mode: "out-in" },
    // Global page headers (https://go.nuxtjs.dev/config-head)
    head: {
      htmlAttrs: {
        lang: "en-GB",
      },
      title: "Scripted Pixels",
      link: [
        {
          rel: "icon",
          type: "image/x-icon",
          href: "/favicon-32.png",
        },
        {
          hid: "canonical",
          rel: "canonical",
          href: "https://www.scriptedpixels.co.uk",
        },
      ],
      script: [
        {
          src: "//static.getclicky.com/js",
          'data-id': "101368919",
          tagPosition: "bodyOpen",
        },
        {
          src: "//scripts.simpleanalyticscdn.com/latest.js",
          tagPosition: "bodyOpen",
          'data-collect-dnt': true,
          async: true,
          defer: true,
        },
      ],
    },
  },

  content: {
    highlight: {
      theme: "monokai",
    },
    markdown: {
      tags: {
        iframe: 'ProseIframe'
      }
    }
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    "@nuxt/content",
    "@nuxtjs/robots",
  ],

  nitro: {
    prerender: {
      failOnError: false,
    },
  },

  vite: {
    plugins: [
      tailwindcss(),
    ],
  },

  css: [
    "./assets/css/tailwind.css",
  ],

  routeRules:{
    '/work/sitesearhai': { redirect: { to:'/work/sitesearchai', statusCode: 301 } },
  },
});
