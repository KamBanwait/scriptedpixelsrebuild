---
title: "Operational Solutions"
description: "Led front-end overhaul of F.A.C.E application, integrating Vue 3, Vue Router, Pinia, JWT authentication, and WebSockets. Collaborated on wireframes, designs, and architectural decisions. Instrumental in MVP development, securing contracts for airspace security against UAVs."
date: "2021-12-01"
dateWorked: "December 2021 - March 2023"
tags: ["Vue", "Web Sockets", "Leaflet", "Drone", "UAV", "DotNet", "JavaScript", "HTML", "CSS", "Responsive", "jQuery", "Freelance"]
category: "Work"
type: Contract
clientLogo: "./images/clientLogos/osl.png"
draft: false
---


Website Link: [Operational Solutions](https://osltechnology.com/face-3/){:target="blank"}

As the Senior Front-end Developer, I led the planning and execution of the front-end overhaul for the F.A.C.E application. Previously a multi-file vanilla JS application, it now incorporates cutting-edge technologies such as Vue 3, Vue Router, Pinia, JWT authentication, and WebSockets. The application relies on real-time sensor data obtained through REST endpoints and web sockets to monitor UAVs (Unmanned Aerial Vehicles) in secure airspaces.

In close collaboration with stakeholders, product owners, and back-end developers, I played a pivotal role in delivering high-level wireframes and designs for each new feature and enhancement. Beyond design, I spearheaded crucial front-end architectural decisions, resulting in the successful development of an MVP (Minimum Viable Product). This MVP was instrumental in securing new customers and contracts, forming the basis for the final production application, to which I also contributed.


Consequently, the client secured contracts for securing airspace against UAV’s.

<img src="/images/work/FACEV3InSitu.jpg" />
