---
title: "Theo Paphitis Retail Group"
description: "Led front-end enhancements for Magento v1 stores including Ryman Stationery, Robert Dyas, and London Graphic Centre. Introduced modern HTML, CSS, and JavaScript features, trained developers, and provided UI/UX guidance for significant projects like new checkout processes and website development."
date: "2016-09-01"
dateWorked: "September 2016 - December 2018"
tags: ["Archived", "Magento 1", "JavaScript", "HTML", "Sass", "Responsive", "jQuery", "Freelance"]
category: "Work"
type: Contract
clientLogo: "./images/clientLogos/theopaphitisretailgroup.png"
draft: false
---

Website Links: [London Graphic Centre](https://www.londongraphics.co.uk), [Ryman](http://ryman.co.uk), [Robert Dyas](https://www.robertdyas.co.uk)

I maintained & improved the front-end for 3 Magento v1 stores: Ryman Stationery, Robert Dyas and London Graphic Centre. I introduced new HTML, JavaScript, React & CSS features and functionality as well as refactoring older templates to ensure one theme was used for all stores, following the parent - child relationship of Magento theming.

As lead Front-end/UI developer I also helped train developers to use newer CSS 3, HTML 5 & JavaScript features. I presented and advised to the business; best UI/UX practices, wireframes and designs for both the new checkout project on Ryman and Robert Days stores, as well as the new Magento store for the London Graphic Centre website.
