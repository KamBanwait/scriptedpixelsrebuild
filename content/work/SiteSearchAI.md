---
title: "SiteSearch.ai"
description: "Led development of a versatile app with VueJS, ensuring seamless functionality and robust security. Secured funding for client's business."
date: "2024-02-01"
dateWorked: "September 2023 - ongoing"
tags: ["A.I", "Jekyll", "Vue", "Pinia", "JWT", "Vue Router", "Responsive", "Freelance"]
category: "Work"
type: Contract
clientLogo: "./images/clientLogos/sitesearch.ai.png"
draft: false
---

Website link: [Sitesearch.ai](https://sitesearch.ai/)

<!-- <iframe src="https://stackblitz.com/edit/vitejs-vite-pfpjwe?embed=1" title="StackBlitz test"></iframe> -->

Sitesearch.ai is an AI-powered search engine that helps users find relevant content based on context.

As the Senior Front-end Developer, I led the creation of the application used by my client and their customers. I oversaw the design and development of the public-facing website and extended the branding to the Vue SPA (Single Page Application).

Collaborating closely with the founder, I constructed the front-end of the application, providing administrative functionality for their customers and enabling system-admins to use the application as customers themselves. This was achieved through "role-based access controls" using JWT (JSON Web Tokens). I implemented a dynamic UI that adjusted functionality and features based on the user's role and access levels.

I chose VueJS for the front-end due to its robust ecosystem: Pinia for state management, Vue-router for authenticated routing, PrimeVue for pre-built components, and Vitest for unit-tests. This application heavily relies on REST, and I leveraged Postman to test and suggest improvements to the data returned to the front-end.

Developing these two distinct sites allowed my client to showcase their concept, facilitating the acquisition of funding for subsequent business and development phases.
