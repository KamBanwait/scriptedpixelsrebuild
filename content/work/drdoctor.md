---
title: "Dr Doctor"
description: "Led migration and development of an NHS Insights Portal using Vue3. Collaborated with experts to create data visualizations and utilized PrimeVue for tabular views. Assisted in debugging and implementing features across the application."
date: "2024-01-20"
dateWorked: "April 2023 - October 2023"
tags: ["Vue", "TypeScript", "GraphQL", "Pinia", "JWT", "A.I", "NHS"]
category: "Work"
type: Contract
clientLogo: "./images/clientLogos/drdoctor.png"
draft: false
---

Website Link: [Dr Doctor](https://www.drdoctor.co.uk)

As a Vue3 Developer, I played a pivotal role in migrating, and developing, an “Insights Portal” for NHS trusts.

I collaborated with machine learning experts, data analysts and designers to develop data visualisations, using D3 & Power BI reports, highlighting areas where trusts were potentially loosing money from patients that didn’t show.

I utilised PrimeVue, as it was used across the larger application, to display a tabular view of the data. I also assisted and collaborated with other areas of the application and front-end developers in the company. Helping to debug, discuss implementations and develop new features.
