---
title: "Next Retail: Childsplay Clothing"
description: "Enhanced e-commerce stores, notably ChildsPlay Clothing, at Next Retail. Conducted A/B tests with Monetate (KIBO) for UX optimization. Mentored juniors, managed releases, and boosted site performance. Demonstrated ability to drive improvements for Next Retail."
date: "2021-09-01"
dateWorked: "September - December 2021"
tags: ["Monetate", "A/B testing", "JavaScript", "HTML", "CSS", "Responsive", "jQuery", "Freelance"]
category: "Work"
type: Contract
clientLogo: "./images/clientLogos/next.png"
draft: false
---

Website Links: [Childsplay Clothing](http://childsplayclothing.co.uk/) [Next](http://next.co.uk)

As a senior front-end developer I worked a short but fast-paced contract to help across multiple e- commerce stores within Next Retail.

My main focus was on ChildsPlay Clothing. I built A/B tests using Monetate (KIBO) to help Next understand what new features they were investigating provided the best user experience and return for the business.

With customers spending large sums of money on kids clothing, the quality and experience had to meet the client’s expected high level of quality.

I guided newer, mid/junior members of the team up-skill by mentoring, testing and code reviewing with helpful feedback where needed.

I also looked after releases for the team as well as presented newer methods of creating faster experiences.
