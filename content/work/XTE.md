---
title: "XTE Ltd"
description: "Development of an MVP test harness application for testing software built to Secure Airspaces. Using Vue.js and LeafletJS for real-time drone monitoring and accuracy assessment. Accessible across devices, with playback support for flight analysis."
date: "2024-01-30"
dateWorked: "September 2023"
tags: ["Vue", "PrimeVue", "Pinia", "JWT", "Vue Router", "Leaflet", "TypeScript", "Vitest", "Responsive", "Freelance", "Drone", "UAV", "Web Sockets"]
category: "Work"
type: Contract
draft: false
clientLogo: "./images/clientLogos/xte.png"
---

<p class="introduction">Lead the front-end development of an MVP test harness application; automating manual processes from an existing application to a modern and portable application.</p>

Website Links: [XTE Ltd](https://x-te.co.uk/){:target="blank"}

Collaborating closely with the client and a back-end developer, I spearhead the front-end development of an MVP test harness application. This application streamlines the testing processes by automating manual steps and adopting a modern and responsive approach. The app assesses the accuracy of my client's customer software for securing airspace. These tests are crucial to verify that companies offering these services meet the required standards for official licensing.

The client is able to monitor how their customer's software detects and displays flight data against test routes, which the client creates in the app. This functionality allows the client to evaluate the accuracy of the Software visually. I developed a visual representation of these flights using [Leafletjs](https://leafletjs.com){:target="blank"}. Leveraging their own sensors, customers detect UAVs and visualise their journeys in real-time on a map. Employing WebSockets, the app receives live data feeds from the back-end to plot flight trajectories. Additionally, the application supports playback of recorded drone flights alongside video feeds, empowering the client to navigate through playback sessions and analyse specific points within a flight.

I developed the app to be accessible & usable from mobile devices, tablets, & desktop computers. Its intended users include drone pilots, spotters in the field, and staff operating from a central or mobile office(s). As users are in the field and rely on a mobile network, speed and minimal application size was crucial. I opted for Vue.js and its complementary ecosystem.

### Demo

<div class="col-span-full my-10">
  <div class="mockup-browser shadow-lg shadow-brand-purple-800 bg-base-200">
    <div class="mockup-browser-toolbar">
      <div class="input">https://www.ndaclientproject.com</div>
    </div>
    <div class="flex justify-center bg-base-200">
      <video muted controls class="w-full " >
        <source src="../../video/DroneTracking264.mov" type="video/mp4">
        Your browser does not support the video tag.
      </video>
    </div>
  </div>
</div>

