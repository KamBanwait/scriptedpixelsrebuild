---
title: "First For Hospitality"
description: "Using Vue.js and Stripe, I enhanced the existing website from manually updating events to fetching events from an API and allowing users to purchase tickets directly."
date: "2021-06-01"
dateWorked: "May 2021 - July 2021"
tags: ["Vue", "APIs", "Stripe", "PHP", "Design", "JavaScript", "HTML", "CSS", "Responsive", "jQuery", "Freelance"]
category: "Work"
type: Contract
clientLogo: "./images/clientLogos/firstForHospitality.png"
draft: false
---

<p class="introduction">I leveraged Vue.js and Stripe to streamline First For Hospitality's customer experience by introducing a seamless ticket listing and purchase flow. This innovative solution eliminated the need for manual event updates and individual invoicing with each ticket purchase. Utilizing Vue.js, I seamlessly retrieved the latest events from an API while seamlessly integrating Stripe's API, empowering customers to securely and conveniently purchase tickets</p>

First For Hospitality is an established events ticket company that provides companies with special tickets for many events. Previously, to purchase tickets, the customer's would search through a list of events (with no filtering) and find the event(s) they're interested in. They would then call First For Hospitality to check availability and then invoice is raised. The customer would then pay the total when that invoice was received via email.

First For Hospitality approached me to help streamline their customer journey with the introduction of a new API from their ticket agent. This new API helps remove the manual process of updating events in WordPress.

#### The previous ticket listing
Below shows how the customer's would previously search through the list of events and find the event(s) they're interested in:

<div class="col-span-full">
  <div class="mockup-browser shadow-lg shadow-brand-purple-800 bg-base-200">
    <div class="mockup-browser-toolbar">
      <div class="input">https://www.firstforhospitality.com/what-we-offer/football-hospitality-tickets</div>
    </div>
    <div class="flex justify-center bg-base-200">
      <video muted controls class="w-full " >
        <source src="../../video/firstForHospitalityPrevious.mov" type="video/mp4">
        Your browser does not support the video tag.
      </video>
    </div>
  </div>
</div>

<div class="col-span-full">
  <div class="divider my-10"></div>
</div>

#### The new ticket listing and purchase flow
Below shows the new ticket search, filter and purchase flow:

<div class="col-span-full">

  <div class="mockup-browser shadow-lg shadow-brand-purple-800 bg-base-200">
    <div class="mockup-browser-toolbar">
      <div class="input">https://www.firstforhospitality.com/what-we-offer/football-hospitality-tickets</div>
    </div>
    <div class="flex justify-center bg-base-200">
      <video muted controls class="w-full " >
        <source src="../../video/firstForHospitalityNew.mov" type="video/mp4">
        Your browser does not support the video tag.
      </video>
    </div>
  </div>
</div>

<small class="mx-auto my-5 block text-center opacity-50">[First For Hospitality: Football tickets](https://www.firstforhospitality.com/what-we-offer/football-hospitality-tickets){:target="blank"}</small>
</div>

The Vue.js app is used for Football, Boxing, and Music. With scope to implement it for other pages when API's are available from the ticket agent. 
