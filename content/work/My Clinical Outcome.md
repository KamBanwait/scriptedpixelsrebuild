---
title: "My Clinical Outcomes"
description: "Explored Angular and React frameworks for MCO's app development, ultimately choosing React. Developed front-end interfacing with a .NET database, ensuring usability on older hospital tablets. Prioritized legibility and accessibility across user demographics with clear call-to-action elements."
date: "2015-12-01"
dateWorked: "December 2015 - April 2016"
tags: ["Archived", "React", "DotNet", "HTML", "Sass", "Responsive", "jQuery", "Freelance"]
category: "Work"
type: Contract
draft: false
---

Website link: [My Clinical Outcomes](https://www.myclinicaloutcomes.com)

Using Webpack, Reactjs, React-MaterialDesignLight, Sass & JSX. I created working proof of concepts for the client which then developed into full web-app pages used to replace their current .net application.

Working closely with the CTO and the lead developer, I was tasked to research and develop proof of concept’s using Facebook’s ReactJs and Google’s AngularJs; recreating the current web app they currently have but using a newer tech stack.

The aim here was to provide an application that worked across multiple screens, became more accessible, worked faster for the end user and was lighter than its current build. Making the front end a truly data-driven interface and leaning towards a single page application feel for users, the general public and administration staff.
