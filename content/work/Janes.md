---
title: "Janes"
description: "Led UI/UX upgrades for an API-driven app, fostering teamwork across departments. Introduced Vue.js in DotNet/MVC, ditching old tech for better efficiency. Used modern JS standards and Sass for styling, making the app more responsive."
date: "2020-05-01"
dateWorked: "May 2020 - September 2021"
tags: ["DotNet", "Vue", "JavaScript", "HTML", "Sass", "Responsive", "jQuery"]
category: "Work"
type: Permanent
clientLogo: "./images/clientLogos/janes.png"
draft: false
---

Website Links: [Janes Portal](http://customer.janes.com), [Janes](http://janes.com)

In my role as a senior front-end software engineer, I spearheaded improvements in the API-driven application's user interface and experience. I effectively collaborated with cross-functional teams, including business, UX experts, and technology specialists, to manage the front-end workload. My contributions facilitated the transformation of the application into a modern and responsive system.

Within the team, I introduced a component-based approach using Vue within the DotNet/MVC framework. This involved phasing out older technologies like Knockout and legacy jQuery in favor of leaner vanilla JavaScript for improved speed and efficiency. Leveraging Gulp and WebPack, I ensured compatibility with older browsers while enabling the adoption of modern JavaScript standards (ES6/Next). Concurrently, I facilitated the transition to Sass for styling, with an emphasis on component-based styling following the BEM methodology.

Furthermore, I collaborated closely with the in-house UI/UX team, actively participating in the design of new page layouts, addressing technology limitations, and proposing alternative implementations to enhance application responsiveness.
