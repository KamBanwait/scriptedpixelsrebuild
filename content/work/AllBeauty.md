---
title: "All Beauty"
description: "Guided design decisions and led developers in crafting templates for the company's Magento store. Implemented Sass for CSS to streamline styling. Collaborated with designers to optimize page components for diverse screen sizes, promoting mobile-first responsive design principles and educating the company on their advantages."
date: "2015-06-01"
dateWorked: "June 2015 - December 2015"
tags: ["Archived", "Magento", "PHP", "HTML", "Sass", "Animation", "Responsive", "jQuery", "Freelance"]
category: "Work"
type: Contract
clientLogo: "./images/clientLogos/allbeauty.png"
draft: false
---

Website link: [All Beauty](https://www.allbeauty.com)

- Helped create a scalable & sustainable Sass/CSS/HMTL/JS codebase within the new Magento theme

- Ensured that the design/theme is consistent throughout the site with the use of Sass and HMTL modules

- Ensured that HTML used throughout the theme is consistent for elements within the theme, such as the HTML used for buttons & form fields.

- Extended the Foundation Grid framework to suit the new design,

- Provided guidance on best practice for user interaction and experience for all screen sizes.

- Took on Google Analytics data to look at what devices users were shopping from mainly and used this to drive decisions for usage on HTML5/CSS3.

- Provided fallbacks for all new HTML/CSS features we decided to implement for older browsers.

- Used my personal device lab for testing as well as the in house QA.

- Startred the creation of a Pattern Library for future developers & designers.
