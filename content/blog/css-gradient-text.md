---
title: "CSS Gradient text"
date: "2024-04-23"
description: "How to create text with a gradient background using CSS"
tags: ["CSS", "HTML"]
category: "Web Development"
draft: false
---

<p class="introduction">
In this short post I'll be showing you how to add a gradient background to text using CSS.
</p>

First of all, we need some text to colour. Lets use the `h1` tag:

```html
<h1>CSS Gradient Text</h1>
```

Now, we can add some CSS to target the h1 and add the properties needed to create the gradient background. For my example below I use the `background` property and set it to a `linear-gradient()` CSS function. This function takes in a direction, example is in degrees but it can take in a direction like '`to right top`', and some colours. Colours can be passed in as hex, rgb, rgba or as a name. We can also set the percentage of which a colour will be shown in the gradient, known as the `color stop`.

The CSS for my example:

`background: linear-gradient(90deg, #ff0000 15%,#3da8f5 30%,#53fa00 45%,#b300ff 60%,#ff5500 75%,#ff0091 100%);`

`background-clip: text;` helps to clip the background to the text.

`color: transparent;` makes the text transparent, revealing the gradient we've set on the background:

```css
h1 {
  background: linear-gradient(90deg, #ff0000 15%,#3da8f5 30%,#53fa00 45%,#b300ff 60%,#ff5500 75%,#ff0091 100%);
  background-clip: text;
  color: transparent;
}
```

### Demo

I've created an interactive demo that outputs the generated CSS. You can change the angle of the gradient, choose different colours and change their colour stop percentages:

<GradientTextWithCss></GradientTextWithCss>

## Wrap up

You can create some complex gradients with the `linear-gradient` function and I'd recommend reading up about the values you can use in the function [here](https://developer.mozilla.org/en-US/docs/Web/CSS/linear-gradient){target="_blank"}.
