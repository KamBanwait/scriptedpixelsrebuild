---
title: "Fira Code as my IDE font"
date: "2022-01-04"
description: "FiraCode is a font that supports ligatures, using this in my IDE makes reading code a lot easier... and it looks cleaner and cooler!"
tags: ["IDE", "CSS", "HTML", "JavaScript"]
category: "Web Development"
draft: false
---

<p class="introduction">FiraCode is a font that supports ligatures, using this in my IDE makes reading code a lot easier... and it looks cleaner and cooler!</p>

```html
<style>
  @import url(https://cdn.jsdelivr.net/npm/firacode@6.2.0/distr/fira_code.css);
  code, code[class*="language-"], pre[class*="language-"] {
    font-family: 'Fira Code';
  }
</style>
```

Download the [Font here](https://github.com/tonsky/FiraCode) and install.

I use both VSCode and WebStorm as my daily IDE's. For both of these I have Fira Code set as the font for the text editor. The advantages of using ligatures are shown below.

How map array looks with ligatures enabled:

<img src="/images/blog/ligatures-example.png" alt="Ligatures examples" class="border-none py-0 my-0">

How map array looks with ligatures disabled:

<img src="/images/blog/ligatures-example-disabled.png" alt="Ligatures examples" class="border-none py-0 my-0">

How ligatures look with common JS:

```js
// Equality:
== ===

// Scope:
=> -> __

// Comparison:
<= >= != !==
```

Best thing with this font is that it works with more than one language. It has support for PHP, Swift, Ruby, and Go.

Example from GitRepo:

<img src="/images/blog/ligatures.png" alt="Ligatures examples" class="border-none py-0 my-0">


You'll need to enable font-ligatures in the IDE settings:

VScode:

<img src="/images/blog/font-ligatures-vscode.png" alt="VSCode ligatures settings" class="border-none py-0 my-0">


WebStorm:

<img src="/images/blog/font-ligatures-webstorm.png" alt="WebStorm ligatures settings" class="border-none py-0 my-0">
