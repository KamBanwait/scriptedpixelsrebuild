---
title: "How to correctly install Supabase in a SwiftUI project"
date: "2024-05-14"
description: "I ran in to some issues when setting up Supabase in a new SwiftUI project. I've documented the issue and solution for future reference."
tags: ['SwiftUI', 'iOS', 'Database']
category: "Web Development"
draft: false
---

<p class="introduction">
I've been keeping an eye on SwiftUI since it was announced at Apple's Developer Conference (WWDC) in 2019<sup>1</sup>. I've always been interested in learning how to make apps for the iPhone, Apple Watch, iPad, and macOS. These devices are everywhere and the Apple ecosystem is super convenient as a user and as a App developer. Using SwiftUI, you can build an application for all of these platforms from one Xcode project! Hearing this and knowing SwiftUI has had time to mature, it feels like a good time to start playing around with Xcode & building an app. I have a <em>real problem</em> that I want to try and resolve, something that all developers recommend you do when trying to learn a new language/framework.</p>


## Why?

I want to log my indoor rowing workouts. The machine I have lacks any sort of modern connectivity. I think you can use a USB cable to connect it to a computer to & then extract data, but who wants to take their laptop to a gym?. Currently, I use my Apple Watch to log my rowing exercises. The issue is that it can't track details that the machine outputs/measures (split time, projected finish, finish time and an average). I want to be able to finish my workout, take a picture of the screen, and have it logged. This will allow me to look at my history and see some pretty charts of progression. The workout would also get logged into the Apple Health app. This will allow me to track heart rate and other metrics within in my overall health data.

<img src="/images/blog/supabase-swiftui-setup/concept2-pm3-display.jpeg" alt="Workout data displayed at the end of a workout on a Concept 2 PM3 screen. 500m row workout" width="500" />

As a Front-end developer, I wasn't looking to get into all the details of setting up a database, API's, and handling requests. Soo, I've decided to use [Supabase](https://supabase.com){target="_blank"} as a database provider. They provide a lot of functionality out of the box. It includes user authentication as well as Row Level Security, I'll need these for when I want to share the app so others can log their workouts. Supabase will also be used for my web-app.


## The Problem

I wanted to start connecting my SwiftUi project to a dummy Supabase database. This would allow me to test the user sign-in and log-in flows and learn how I work with API calls within SwiftUI. However, after closely following [the guide](https://supabase.com/docs/guides/getting-started/tutorials/with-swif){target="_blank"} in the Supabase docs, I ran in to an issue. The 'Supabase-Swift' dependency was being installed but not recognised in my project. I spent just under an hour debugging the issue and found the solution.

_Installing SwiftUI dependencies is ver much like installing a Node dependency. You find a library from a third party that provides a specific feature/solution. Just like web-apps, sometimes these packages need a little more work to get up and running in a project._

The error message being displayed after installing the package:

<img src="/images/blog/supabase-swiftui-setup/error.jpg" alt="Error message showing the project couldn't find the newly installed Supabase package" />


## The solution

Just like Node packages, there's a chance that a configuration file needs some changes made to it that's missed during the installation or is an un-documented change in a recent release.

There's many areas in an Xcode project that display information about the app you're building. What I found is that the Supabase package wasn't being registered within the "Frameworks, Libraries, and Embedded Content":

<img src="/images/blog/supabase-swiftui-setup/before.jpg" alt="Frameworks, Libraries, and Embedded Content" />

_You can see that I'm targeting iPhone, iPad and macOS here. macOS will be removed until I've got the app working on iOS and iPad._

I had to manually register the Supabase dependency in to this list. Clicking "+" icon allows me to see a list of dependencies:

<img src="/images/blog/supabase-swiftui-setup/solution-step-1.jpg" alt="Filtering frameworks and Libraries to the Supabase package" />

Clicking "Add" inserted Supabase into the app's "Frameworks, Libraries, and Embedded Content" list:

<img src="/images/blog/supabase-swiftui-setup/solution-step-2.jpg" alt="Updated list of frameworks, Libraries, and Embedded Content that now shows Supabase" />

Checking my main View now shows that the project is able to find the Supabase package 🥳:

<img src="/images/blog/supabase-swiftui-setup/error-no-more.jpg" alt="No error message stating the project couldn't find the Supabase package" />

Xcode is now able to render a real time preview of my bare-bones application in the preview window. This is very much like HMR (hot  module reloading) when developing websites and seeing the browser reload each time a change is made 😍:

<img src="/images/blog/supabase-swiftui-setup/working.jpg" alt="Xcode showing the project running with a single input text field for email address and a Sign in button." />


## Wrap up

What seems like a simple fix, took a lot of time for me to get my head around as it's the first time I've started to really build out anything meaningful in Xcode. But, these pain points are where developers learn. I know more about Xcode and how apps are structured.

The process of putting together a UI then implementing the communication via API calls to Supabase feels very much like building a Single Page Application. I'm hoping the rest of the app build feels just as familiar.


#### References:
1 [WWDC 2019 videos](https://developer.apple.com/videos/wwdc2019){target="_blank"}
