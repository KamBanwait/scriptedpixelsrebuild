---
title: "Setting up Docker on Apple Silicon"
date: "2023-05-28"
description: "How to setup Docker on Apple Silicon"
tags: ["Apple", "Web Dev", "Setup", "Work Space"]
category: "Web Development"
draft: true
---

<p class="introduction">How to setup Wordpress in Docker on Apple Silicon</p>

I've struggled to work on WordPress sites locally with Mamp after using Docker, and a CI/CD flow, at various jobs. I've decided to write up the steps taken to get WordPress setup. This is for a fresh WordPress install.

### Install Docker desktop

I'm assuming you know what Docker is and what containers are. You'll need Docker desktop installed. This installs Docker with Docker compose. Download and install Docker for MacOS (M1) [here](https://docs.docker.com/desktop/install/mac-install){:target="\_blank"}

### Create a docker-compose file

Create a directory where you usually have you websites setup. I've got mine in `~/Users/scriptedpixels/websites/`. I've created a directory in here called: `/clientNameDocker/`. I'll also be adding the existing site files to this directory.

In this directory we need a file called `docker-compose.yml`, make sure you create it along side your `wp-content` directory. This is important.

The contents of `docker-compose` will contain the services Docker needs to spin up for us to work on our Wordpress site. I've created the below from various tutorials. More details about the file can be found [here](https://docs.docker.com/compose/compose-file/03-compose-file){:target="\_blank"}.

The DataBase, MySQL is the first service to be created, as it's WordPress:

```yaml
# Tells Docker what version of Compose we're using
version: "3.8"

# Services line define which Docker images to run. In this case, it will be MySQL server and WordPress image.
services:
  db:
    container_name: "local-wordpress-db"
    image: mysql:5.7 # image: mysql:5.7 indicates the MySQL database container image from Docker Hub used in this installation.
    ports: # Maps a server port from your computer to a server port inside of the container.
      - 18766:3306
    volumes: # Maps folders from your computer to folders inside of the containe
      - db_data:/var/lib/mysql
    restart: always # Restart line controls the restart mode, meaning if the container stops running for any reason, it will restart the process immediately.
    environment: # Sets some environment variables inside of the container
      MYSQL_ROOT_PASSWORD: rootpassword
      MYSQL_DATABASE: WordPressDatabaseName
      MYSQL_USER: root
      MYSQL_PASSWORD: root
      # Previous four lines define the main variables needed for the MySQL container to work: database, database username, database user password, and the MySQL root password.
```

The next service we tell Docker to run is WordPress:

```yaml
---
wordpress:
  container_name: "local-wordpress"
  depends_on: # This option tells Docker Compose that the db container should be started before this container can be started.
    - db
  image: wordpress:6.2 # use 'latest, or the specific version your client is running
  ports:
    - "8000:80" # Defines the port that the WordPress container will use. After successful installation, the full path will look like this: http://localhost:8000
  restart: always # Restart line controls the restart mode, meaning if the container stops running for any reason, it will restart the process immediately.
  environment:
    WORDPRESS_DB_HOST: db:3306
    WORDPRESS_DB_USER: MyWordPressUser
    WORDPRESS_DB_PASSWORD: Pa$$w0rD
    WORDPRESS_DB_NAME: MyWordPressDatabaseName
    # Similar to MySQL image variables, the last four lines define the main variables needed for the WordPress container to work properly with the MySQL container.
  volumes: ["./:/var/www/html"]
```

Your complete yaml file will look like this:

```yaml
services:
  # Services line define which Docker images to run. In this case, it will be MySQL server and WordPress image.
  db:
    platform: linux/amd64
    image: mysql:5.7
    # image: mysql:5.7 indicates the MySQL database container image from Docker Hub used in this installation.
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: MyR00tMySQLPa$$5w0rD
      MYSQL_DATABASE: MyWordPressDatabaseName
      MYSQL_USER: MyWordPressUser
      MYSQL_PASSWORD: Pa$$5w0rD
      # Previous four lines define the main variables needed for the MySQL container to work: database, database username, database user password, and the MySQL root password.
  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    restart: always
    # Restart line controls the restart mode, meaning if the container stops running for any reason, it will restart the process immediately.
    ports:
      - "8000:80"
      # The previous line defines the port that the WordPress container will use. After successful installation, the full path will look like this: http://localhost:8000
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: MyWordPressUser
      WORDPRESS_DB_PASSWORD: Pa$$5w0rD
      WORDPRESS_DB_NAME: MyWordPressDatabaseName
    # Similar to MySQL image variables, the last four lines define the main variables needed for the WordPress container to work properly with the MySQL container.
    volumes: ["./:/var/www/html"]
```

### Starting the containers

Navigate to where you've just set this up in your terminal window. The run the following command to tell Docker to spin up the services: `docker-compose up -d`. When this has told you it's finished, head on over to `http://localhost:8000` in your browser. You should be shown the wordpress install wizard.

## Conclusion

Setting up a WordPress development environment is quick and easy with Docker. We just need to add a `docker-compose.yml` config file and then add a couple of predefined container images (services).

Because it’s so quick and easy, Docker is a far superioir alternative to approaches like Vagrant, as you have to deal with virtual machines. I always found that something would break when spinning up Vagrant after short breaks from developing websites that required less frequent updates.

The next step here is to set up Wordpress as a new site or import a previous WordPress install
