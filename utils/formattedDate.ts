// It will be available as randomEntry() (camelCase of file name without extension)
export default (dateToFormat: Date) => {
  const options: Intl.DateTimeFormatOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  const dateTimeFormat = new Intl.DateTimeFormat("en-gb", options);
  const date = new Date(dateToFormat);
  const formatDate = dateTimeFormat.format(date);

  return formatDate;
};
