const useGlobalStore = () => {
  const navigationLinks = [
    {
      name: "Home",
      to: 'index',
      color: 'Red'
    },
    {
      name: "About",
      to: 'about',
      color: 'Blue'
    },
    {
      name: "Work",
      to: 'work',
      color: 'Green'
    },
    {
      name: "Blog",
      to: 'blog',
      color: 'Purple'
    },
  ];

  return {
    navigationLinks,
  }
}

export {
  useGlobalStore
}
